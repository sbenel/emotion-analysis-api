from transformers import DistilBertTokenizerFast
from transformers import TFDistilBertForSequenceClassification
import tensorflow as tf
import json
import pathlib
from translate import translate_text

def load_model():
    tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')
    modelpath = str(pathlib.Path(__file__).parent.absolute()) + '/sentiment_custom_model/'
    loaded_model = TFDistilBertForSequenceClassification.from_pretrained(modelpath)
    return tokenizer, loaded_model

def predict(data, tokenizer, model):

    formated_data = singleQuoteToDoubleQuote(str(data))

    test_sentence = json.loads(formated_data)
    spanish_message = translate_text(test_sentence['message'])

    predict_input = tokenizer.encode(spanish_message,
                                 truncation=True,
                                 padding=True,
                                 return_tensors="tf")
    tf_output = model(predict_input)[0]


    tf_prediction = tf.nn.softmax(tf_output, axis=1).numpy()[0]

    emo_dict = {
        "sad": tf_prediction[0],
        "joy":tf_prediction[1],
        "love": tf_prediction[2],
        "anger": tf_prediction[3],
        "fear": tf_prediction[4],
        "surprise": tf_prediction[5]
        }

    result = dict(sorted(emo_dict.items(), key=lambda item: item[1], reverse=True))

    return str(result)

def singleQuoteToDoubleQuote(singleQuoted):
            cList=list(singleQuoted)
            inDouble=False;
            inSingle=False;
            for i,c in enumerate(cList):
                if c=="'":
                    if not inDouble:
                        inSingle=not inSingle
                        cList[i]='"'
                elif c=='"':
                    inDouble=not inDouble
            doubleQuoted="".join(cList)
            return doubleQuoted

