from flask import Flask
from flask import request
import json
from distilbert import predict 
from translate import translate_text
from distilbert import load_model


app = Flask(__name__)

tokenizer, model = load_model()

@app.route("/")
def index():
    return "<p>Hello, World!</p>"

@app.route("/distilbert", methods=['POST'])
def distilbert():
    values = request.get_json()
    result = predict(values['data'], tokenizer, model)
    res = str(json.dumps(result)).replace("'","\"")
    return res

@app.route("/translate", methods=['POST'])
def translate():
    values = request.get_json()
    result = translate_text(values['text'])
    return result
