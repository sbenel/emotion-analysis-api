from ibm_watson import LanguageTranslatorV3
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
import json

authenticator = IAMAuthenticator('JyYNpF3rt3CWlzYLdwKaISuuy4cnEofbXE4-V---M3E9')
language_translator = LanguageTranslatorV3(
    version='2018-05-01',
    authenticator=authenticator
)

language_translator.set_service_url('https://api.us-south.language-translator.watson.cloud.ibm.com/instances/864f2320-8292-4973-a69e-f62cfa7303cb')

def translate_text(original_text):
    translation = language_translator.translate(
            text=original_text,
            model_id='es-en').get_result()
    return translation['translations'][0]['translation']



